#---------------------------------------------------------------------------------
# Clear the implicit built in rules
#---------------------------------------------------------------------------------
.SUFFIXES:
#---------------------------------------------------------------------------------
ifeq ($(strip $(DEVKITPPC)),)
$(error "Please set DEVKITPPC in your environment. export DEVKITPPC=<path to>devkitPPC")
endif

#include $(DEVKITPPC)/gamecube_rules
include $(DEVKITPPC)/wii_rules

#---------------------------------------------------------------------------------
# TARGET is the name of the output
# BUILD is the directory where object files & intermediate files will be placed
# SOURCES is a list of directories containing source code
# INCLUDES is a list of directories containing extra header files
#---------------------------------------------------------------------------------
TARGET		:=	stepmania
BUILD		:=	output
SOURCES		:=	. \
		arch \
		arch/ArchHooks \
		arch/Dialog \
		arch/InputHandler \
		arch/LoadingWindow \
		arch/LowLevelWindow \
		arch/MemoryCard \
		arch/MovieTexture \
		arch/Sound \
		arch/Threads \
		archutils/Wii \
		pcre \
		libresample/src
DATA		:=	
INCLUDES	:= . \
		arch \
		arch/ArchHooks \
		arch/Dialog \
		arch/InputHandler \
		arch/LoadingWindow \
		arch/LowLevelWindow \
		arch/MemoryCard \
		arch/MovieTexture \
		arch/Sound \
		arch/Threads \
		archutils/Wii \
		pcre \
		libresample/include
		
DEFINE		:= -DHAVE_CONFIG_H -DWII

#---------------------------------------------------------------------------------
# options for code generation
#---------------------------------------------------------------------------------

CFLAGS	= -g -O2 -Wall -Wno-switch $(MACHDEP) $(INCLUDE) $(DEFINE)
CXXFLAGS	=	$(CFLAGS) -save-temps -Xassembler -aln=$@.lst

LDFLAGS	=	-g $(MACHDEP) -Wl,-Map,$(notdir $@).map -Xlinker -Map=$(TARGET).map

#---------------------------------------------------------------------------------
# any extra libraries we wish to link with the project
#---------------------------------------------------------------------------------
LIBS	:= -ldb -lmetaphrasis -lwiiuse -lbte -llua -lfat -lasnd -logc -lm -lz -lpng -ljpeg -lmad -lvorbisidec 

#---------------------------------------------------------------------------------
# list of directories containing libraries, this must be the top level containing
# include and lib
#---------------------------------------------------------------------------------
LIBDIRS	:= $(DEVKITPPC)

#---------------------------------------------------------------------------------
# no real need to edit anything past this point unless you need to add additional
# rules for different file extensions
#---------------------------------------------------------------------------------
ifneq ($(BUILD),$(notdir $(CURDIR)))
#---------------------------------------------------------------------------------

export OUTPUT	:=	$(CURDIR)/$(TARGET)

export VPATH	:=	$(foreach dir,$(SOURCES),$(CURDIR)/$(dir)) \
					$(foreach dir,$(DATA),$(CURDIR)/$(dir))

export DEPSDIR	:=	$(CURDIR)/$(BUILD)

#---------------------------------------------------------------------------------
# automatically build a list of object files for our project
#---------------------------------------------------------------------------------
CFILES		:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.c)))
CPPFILES	:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.cpp)))
sFILES		:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.s)))
SFILES		:=	$(foreach dir,$(SOURCES),$(notdir $(wildcard $(dir)/*.S)))
BINFILES	:=	$(foreach dir,$(DATA),$(notdir $(wildcard $(dir)/*.*)))

#---------------------------------------------------------------------------------
# use CXX for linking C++ projects, CC for standard C
#---------------------------------------------------------------------------------
ifeq ($(strip $(CPPFILES)),)
	export LD	:=	$(CC)
else
	export LD	:=	$(CXX)
endif

#----------------------------------------------------------------
# StepMania objects
#----------------------------------------------------------------
export OFILES := \
	ActiveAttackList.o Actor.o ActorCommands.o ActorFrame.o ActorScroller.o \
	ActorUtil.o AnnouncerManager.o arch.o ArchHooks.o \
	Dialog.o InputHandler.o InputHandler_Wii.o MemoryCardDriver.o \
	MemoryCardDriverThreaded.o MovieTexture.o MovieTexture_Null.o \
	RageSoundDriver_Wii.o RageUtil_CircularBuffer.o \
	ArrowBackdrop.o ArrowEffects.o Attack.o AttackDisplay.o Background.o Banner.o \
	BannerCache.o BeginnerHelper.o BGAnimation.o BGAnimationLayer.o BitmapText.o \
	Bookkeeper.o BPMDisplay.o CatalogXml.o Character.o CharacterHead.o CodeDetector.o \
	CombinedLifeMeterEnemy.o CombinedLifeMeterTug.o Combo.o ComboGraph.o ConditionalBGA.o \
	Course.o CourseContentsList.o CourseEntryDisplay.o CourseUtil.o \
	DancingCharacters.o DateTime.o Difficulty.o DifficultyDisplay.o \
	DifficultyIcon.o DifficultyList.o DifficultyMeter.o DifficultyRating.o DualScrollBar.o \
	EditCoursesMenu.o EditCoursesSongMenu.o EditMenu.o EnumHelper.o FadingBanner.o Font.o \
	FontCharAliases.o FontCharmaps.o FontManager.o Foreground.o Game.o GameConstantsAndTypes.o \
	GameInput.o GameManager.o GameSoundManager.o GameState.o GhostArrow.o GhostArrowRow.o \
	global.o Grade.o GradeDisplay.o GraphDisplay.o GrooveGraph.o GrooveRadar.o GroupList.o \
	HelpDisplay.o HighScore.o HoldGhostArrow.o HoldJudgment.o IniFile.o InputFilter.o \
	InputMapper.o InputQueue.o Inventory.o Judgment.o JukeboxMenu.o LifeMeterBar.o \
	LifeMeterBattery.o ListDisplay.o LyricDisplay.o LyricsLoader.o \
	MemoryCardDisplay.o MemoryCardManager.o MenuTimer.o MeterDisplay.o ModeChoice.o \
	Model.o ModelManager.o ModelTypes.o ModeSwitcher.o MsdFile.o MusicBannerWheel.o \
	MusicList.o MusicSortDisplay.o MusicWheel.o MusicWheelItem.o NoteData.o NoteDataUtil.o \
	NoteDataWithScoring.o NoteDisplay.o NoteField.o NoteFieldPositioning.o NoteSkinManager.o \
	NotesLoader.o NotesLoaderBMS.o NotesLoaderDWI.o NotesLoaderKSF.o NotesLoaderSM.o \
	NotesWriterDWI.o NotesWriterSM.o NoteTypes.o OptionIcon.o OptionIconRow.o OptionsCursor.o \
	PaneDisplay.o PercentageDisplay.o Player.o PlayerAI.o PlayerNumber.o PlayerOptions.o \
	PrefsManager.o Profile.o ProfileManager.o ProTimingDisplay.o RadarValues.o \
	RageBitmapTexture.o RageDisplay.o RageDisplay_Null.o RageException.o RageFile.o \
	RageFileDriver.o RageFileDriverDirect.o RageFileDriverDirectHelpers.o RageFileDriverMemory.o \
	RageFileDriverZip.o RageFileManager.o RageInput.o RageInputDevice.o RageLog.o RageMath.o \
	RageModelGeometry.o RageSound.o RageSoundManager.o RageSoundPosMap.o \
	RageSoundReader_FileReader.o RageSoundReader_MP3.o RageSoundReader_Preload.o \
	RageSoundReader_Resample.o RageSoundReader_Resample_Fast.o RageSoundReader_Resample_Good.o \
	RageSoundReader_Vorbisfile.o RageSoundReader_WAV.o RageSoundResampler.o RageSurface.o \
	RageSurfaceUtils.o RageSurfaceUtils_Palettize.o RageSurface_Load.o RageSurface_Load_BMP.o \
	RageSurface_Load_GIF.o RageSurface_Load_JPEG.o RageSurface_Load_PNG.o RageSurface_Load_XPM.o \
	RageSurface_Save_BMP.o RageTexture.o RageTextureID.o RageTextureManager.o RageThreads.o \
	RageTimer.o RageUtil.o RageUtil_BackgroundLoader.o RageUtil_CharConversions.o \
	RageUtil_FileDB.o RandomSample.o ReceptorArrow.o ReceptorArrowRow.o ScoreDisplay.o \
	ScoreDisplayBattle.o ScoreDisplayNormal.o ScoreDisplayOni.o ScoreDisplayPercentage.o \
	ScoreDisplayRave.o ScoreKeeperMAX2.o ScoreKeeperRave.o Screen.o ScreenAttract.o \
	ScreenBookkeeping.o ScreenBranch.o ScreenCaution.o ScreenCenterImage.o \
	ScreenClearBookkeepingData.o ScreenClearMachineStats.o ScreenCredits.o \
	ScreenDemonstration.o ScreenDownloadMachineStats.o ScreenEdit.o ScreenEditCoursesMenu.o \
	ScreenEditMenu.o ScreenEnding.o ScreenEndlessBreak.o ScreenEvaluation.o ScreenExit.o \
	ScreenEz2SelectMusic.o ScreenEz2SelectPlayer.o ScreenGameOver.o ScreenGameplay.o \
	ScreenHowToPlay.o ScreenInsertCredit.o ScreenInstructions.o ScreenJukebox.o \
	ScreenJukeboxMenu.o ScreenLogo.o ScreenManager.o ScreenMiniMenu.o \
	ScreenMusicScroll.o ScreenNameEntry.o ScreenNameEntryTraditional.o \
	ScreenOptions.o ScreenOptionsMaster.o ArchHooks_Wii.o \
	ScreenOptionsMasterPrefs.o ScreenPlayerOptions.o ScreenProfileOptions.o ScreenPrompt.o \
	ScreenRanking.o ScreenReloadSongs.o ScreenResetToDefaults.o ScreenSandbox.o ScreenSelect.o \
	ScreenSelectCharacter.o ScreenSelectDifficulty.o ScreenSelectGroup.o ScreenSelectMaster.o \
	ScreenSelectMode.o ScreenSelectMusic.o ScreenSelectStyle.o ScreenSetTime.o \
	ScreenSongOptions.o ScreenStage.o ScreenStyleSplash.o ScreenTest.o ScreenTestFonts.o \
	ScreenTestInput.o ScreenTestSound.o ScreenTextEntry.o ScreenTitleMenu.o \
	ScreenUnlock.o ScreenWithMenuElements.o ScrollBar.o ScrollingList.o SnapDisplay.o Song.o \
	SongCacheIndex.o SongCreditDisplay.o SongManager.o SongOptions.o SongUtil.o Sprite.o \
	StageStats.o StepMania.o Steps.o StepsUtil.o Style.o StyleUtil.o TextBanner.o \
	ThemeManager.o TimingData.o TitleSubstitution.o Trail.o TrailUtil.o Transition.o \
	UnlockSystem.o WheelNotifyIcon.o XmlFile.o SDL_rotozoom.o SDL_dither.o LuaHelpers.o \
	AssertionHandler.o Backtrace.o BacktraceNames.o CrashHandler.o CrashHandlerChild.o \
	EmergencyShutdown.o GetSysInfo.o SignalHandler.o Threads_Wii.o chartables.o \
	get.o maketables.o pcre.o study.o RageDisplay_GX.o RageSoundDriver_Null.o RageSoundDriver_Generic_Software.o \
	filterkit.o hackedresamplesubs.o resample.o resamplesubs.o

#---------------------------------------------------------------------------------
# build a list of include paths
#---------------------------------------------------------------------------------
export INCLUDE	:=	$(foreach dir,$(INCLUDES), -iquote $(CURDIR)/$(dir)) \
					$(foreach dir,$(LIBDIRS),-I$(dir)/include) \
					-I$(CURDIR)/$(BUILD) \
					-I$(LIBOGC_INC)

#---------------------------------------------------------------------------------
# build a list of library paths
#---------------------------------------------------------------------------------
export LIBPATHS	:=	$(foreach dir,$(LIBDIRS),-L$(dir)/lib) \
					-L$(LIBOGC_LIB)

export OUTPUT	:=	$(CURDIR)/$(TARGET)
.PHONY: $(BUILD) clean

#---------------------------------------------------------------------------------
$(BUILD):
	@[ -d $@ ] || mkdir -p $@
	@$(MAKE) --no-print-directory -C $(BUILD) -f $(CURDIR)/Makefile

#---------------------------------------------------------------------------------
clean:
	@echo clean ...
	@rm -fr $(BUILD) $(OUTPUT).elf $(OUTPUT).dol

#---------------------------------------------------------------------------------
run:
	wiiload $(TARGET).dol
	
#---------------------------------------------------------------------------------
emulate:
	gcube $(TARGET).dol


#---------------------------------------------------------------------------------
else

DEPENDS	:=	$(OFILES:.o=.d)

#---------------------------------------------------------------------------------
# main targets
#---------------------------------------------------------------------------------
$(OUTPUT).dol: $(OUTPUT).elf
$(OUTPUT).elf: $(OFILES)

-include $(DEPENDS)

#---------------------------------------------------------------------------------
endif
#---------------------------------------------------------------------------------
