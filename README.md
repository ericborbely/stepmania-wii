*Note:*
Since Google Code is closing, I just migrated this repository to BitBucket. Unfortunately there have been no updates :-(. 



[StepMania](http://www.stepmania.com) is a music/rhythm game. The player presses different buttons in time to the music and to note patterns that scroll across the screen. Features 3D graphics, visualizations, support for gamepads/dance pads, a step recording mode, and more! (From original project's SourceForge page)


*Update:*

The project has been revived!  I found this project when I accidentally bought two GC dance pads and wanted to play StepMania on my own Wii.  I saw that the project hadn't been touched in two years and thought I'd take a stab at finishing the port myself!  I've completed the majority of the graphics driver and have been struggling to get the sound driver to work for a couple of weeks now.

I've only tested the 2D drawing of the graphics driver (ya know... the main part) and have everything but the health bar working!  I need to poke around and see what the health bar is trying to do that I've missed and then the game will be playable on the graphics side.

As for the audio... I've bounced back and forth between ASND and AESND but haven't been able to get anything stabilized.  They both crash after a few second of audio playback which leads me to believe that there is something in the engine that isn't very happy with the limited environment on the Wii.

I've removed a lot of the game that wasn't necessary on the Wii or that I just didn't think was realistic for this port (such as the networked play).

While the game is pointless without the music I've uploaded a working build that will fake the audio driver's progress to allow the game to function.

-ObsidianX