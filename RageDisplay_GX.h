
#ifndef RAGEDISPLAY_GX_H
#define RAGEDISPLAY_GX_H

#include <gccore.h>
#include "RageDisplay.h"

#define MAX_TEXTURES 1024

class RageDisplay_GX : public RageDisplay
{
public:
	RageDisplay_GX();
	~RageDisplay_GX();
	void Update(float fDeltaTime);

	void ResolutionChanged();
	const PixelFormatDesc *GetPixelFormatDesc(PixelFormat pf) const;

	bool BeginFrame();	
	void EndFrame();
	VideoModeParams GetVideoModeParams() const;
	void SetBlendMode( BlendMode mode );
	bool SupportsTextureFormat( PixelFormat pixfmt, bool realtime=false );
	unsigned CreateTexture( 
		PixelFormat pixfmt, 
		RageSurface* img, 
		bool bGenerateMipMaps );
	void UpdateTexture( 
		unsigned uTexHandle, 
		RageSurface* img,
		int xoffset, int yoffset, int width, int height 
		);
	void DeleteTexture( unsigned uTexHandle );
	void ClearAllTextures();
	void SetTexture( int iTextureUnitIndex, RageTexture* pTexture );
	void SetTextureModeModulate();
	void SetTextureModeGlow( GlowMode m=GLOW_WHITEN );
	void SetTextureModeAdd();
	void SetTextureWrapping( bool b );
	int GetMaxTextureSize() const;
	void SetTextureFiltering( bool b);
	bool IsZWriteEnabled() const;
	bool IsZTestEnabled() const;
	void SetZWrite( bool b );
	void SetZTestMode( ZTestMode mode );
	void ClearZBuffer();
	void SetCullMode( CullMode mode );
	void SetAlphaTest( bool b );
	void SetMaterial( 
		const RageColor &emissive,
		const RageColor &ambient,
		const RageColor &diffuse,
		const RageColor &specular,
		float shininess
		);
	void SetLighting( bool b );
	void SetLightOff( int index );
	void SetLightDirectional( 
		int index, 
		const RageColor &ambient, 
		const RageColor &diffuse, 
		const RageColor &specular, 
		const RageVector3 &dir );

	void SetSphereEnironmentMapping( bool b );

	RageCompiledGeometry* CreateCompiledGeometry();
	void DeleteCompiledGeometry( RageCompiledGeometry* p );
	
	void EnableConsole(bool enable);

protected:
	void DrawQuadsInternal( const RageSpriteVertex v[], int iNumVerts );
	void DrawQuadStripInternal( const RageSpriteVertex v[], int iNumVerts );
	void DrawFanInternal( const RageSpriteVertex v[], int iNumVerts );
	void DrawStripInternal( const RageSpriteVertex v[], int iNumVerts );
	void DrawTrianglesInternal( const RageSpriteVertex v[], int iNumVerts );
	void DrawCompiledGeometryInternal( const RageCompiledGeometry *p, int iMeshIndex );

	CString TryVideoMode( VideoModeParams params, bool &bNewDeviceOut );
	RageSurface* CreateScreenshot();
	void SetViewport(int shift_left, int shift_down);
	RageMatrix GetOrthoMatrix( float l, float r, float b, float t, float zn, float zf ); 
	RageMatrix GetFrustumMatrix( float l, float r, float b, float t, float zn, float zf ); 
	RageMatrix GetPerspectiveMatrix( float fovy, float aspect, float zn, float zf);

	void SendCurrentMatrices();

private:
	unsigned int FindFreeTextureSlot();
	
	void *mXFB[3];
	unsigned int mFB;
	bool mFirstFrame;
	bool mTexWrap;
	bool mZTest;
	bool mZWrite;
	bool mBeginFrame;
	void *mGPFIFO;
	bool mSwapBuffer;

	class GXTexture {
	public:
		int index;
		int xoffset;
		int yoffset;
		GXTexObj *texture;
		uint32_t *pixels;
	};
	GXTexture *mTextures[MAX_TEXTURES];
};

#endif
