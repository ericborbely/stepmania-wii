#ifndef ARCH_HOOKS_WII_H
#define ARCH_HOOKS_WII_H

#include "ArchHooks.h"

#include <ctime>

class ArchHooks_Wii : public ArchHooks
{
public:
	ArchHooks_Wii();

	static int64_t GetMicrosecondsSinceStart(bool);
};

#undef ARCH_HOOKS
#define ARCH_HOOKS ArchHooks_Wii

#endif
