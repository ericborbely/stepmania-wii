#ifndef RAGE_SOUND_DRIVER_WII_H
#define RAGE_SOUND_DRIVER_WII_H

#include "global.h"
#include "RageSound.h"
#include "RageSoundDriver.h"
#include "RageThreads.h"

#include <gctypes.h>
#include <asndlib.h>

#define MAX_SND_VOICES 16
#define SND_BUFFER (4*1024)
#define SND_BUFFER_SIZE (sizeof(s16)*SND_BUFFER)

class RageSoundDriver_Wii : public RageSoundDriver {
public:
	RageSoundDriver_Wii();
	~RageSoundDriver_Wii();
	
	void StartMixing(RageSoundBase *snd);
	void StopMixing(RageSoundBase *snd);
	int64_t GetPosition(const RageSoundBase *snd) const;

	int GetSampleRate(int rate) const { return 48000; }
	
	struct wiivoice {
	public:
		wiivoice() : mutex("wiivoice") {}
		
		bool active;
		RageSoundBase *sound;
		void *buffer[2];
		bool bufferReady;
		int activeBuffer;
		int bufferCount;
		u64 position;
		bool eof;
		RageMutex mutex;
	};

private:
	RageMutex mMutex;
	RageThread mBufferThread;
};

#endif
