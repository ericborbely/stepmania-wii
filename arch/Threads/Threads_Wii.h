#ifndef THREADS_WII
#define THREADS_WII

#include <ogc/lwp.h>
#include <ogc/mutex.h>
#include <ogc/cond.h>
#include "Threads.h"

class SemaImpl_Wii : public SemaImpl {
   public:
      SemaImpl_Wii(int iInitialValue);
      ~SemaImpl_Wii();
      int GetValue() const {return mValue;}
      void Post();
      bool Wait();
      bool TryWait();
   
   private:
      int mValue;
      mutex_t mMutex;
      cond_t mCond;
};

class ThreadImpl_Wii : public ThreadImpl {
   public:
      void Halt(bool kill);
      void Resume();
      uint64_t GetThreadId() const;
      int Wait();
   
      SemaImpl_Wii *semaphore;
      lwp_t thread;
   
      int (*function)(void *data);
      void *args;
      uint64_t *threadID;
};

class MutexImpl_Wii : public MutexImpl {
   public:
      MutexImpl_Wii(RageMutex *parent);
      ~MutexImpl_Wii();
   
      bool Lock();
      bool TryLock();
      void Unlock();
   
   private:
      mutex_t mMutex;
};

#endif
